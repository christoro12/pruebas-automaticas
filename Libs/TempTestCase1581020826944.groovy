import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner
import com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\JHONSA~1\\AppData\\Local\\Temp\\Katalon\\Test Cases\\5.SAC\\Generales\\5.Informes\\1.Informe_Reporte general Opiniones por USUARIOS\\2.Informe_Reporte general Opiniones por USUARIOS_E_PDF\\20200206_152706\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/5.SAC/Generales/5.Informes/1.Informe_Reporte general Opiniones por USUARIOS/2.Informe_Reporte general Opiniones por USUARIOS_E_PDF', new TestCaseBinding('Test Cases/5.SAC/Generales/5.Informes/1.Informe_Reporte general Opiniones por USUARIOS/2.Informe_Reporte general Opiniones por USUARIOS_E_PDF',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
