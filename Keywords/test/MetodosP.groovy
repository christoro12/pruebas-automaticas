package test

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.Toolkit
import java.awt.event.KeyEvent

import java.awt.datatransfer.StringSelection

import org.eclipse.persistence.internal.oxm.record.json.JSONParser.object_return
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.By.ById
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.Select;

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import sun.net.www.content.text.plain

public class MetodosP {

	@Keyword
	public void select2(TestObject objeto, int sel){

		//Aqui obtenemos una lista de acuerdo a las propiedades que tenga el Objeto
		List<WebElement> elements = WebUiCommonHelper.findWebElements(objeto, 1)
		//Aqui Mostramos por el Log Viewer la cantidad de elementos que llegaron en la lista...
		WebUI.comment('Numero de Elementos' + elements.size())

		int num = 0
		int num1 = 0

		//El bucle simplemente itera la lista para saber que items llegaron...
		for(int i = 0; i < elements.size(); i++){
			num1 = (num + i)
			WebUI.comment('Elemento #: ' + num + ' Es ' + elements[i])
		}

		//Se crea un Elemento Web(WebElement) el cual se encargara de seleccionar el item requerido...
		WebElement Item = elements.get(sel)
		WebUI.delay(5)
		Item.click()

		WebUI.delay(5)

	}

	@Keyword
	public void tiny1(TestObject obj1, int num){
		WebDriver driver3 = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver3, 20)
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName('iframe')))

		List<WebElement> iframe = WebUiCommonHelper.findWebElements(obj1, 5)
		WebUI.comment('Elemento..' + iframe.size())
		driver3.switchTo().frame(iframe.get(num))
		WebUI.comment('Numero de Elementos:  ' + iframe.size())

		WebElement item = driver3.findElement(By.xpath('//*[@id="tinymce"]'))
		String desc = 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla porttitor accumsan tincidunt. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.'
		item.sendKeys(desc)
		driver3.switchTo().defaultContent()
	}

	@Keyword
	public void tinyFormula(TestObject obj2){
		WebDriver driver4 = DriverFactory.getWebDriver()
		WebDriverWait wait1 = new WebDriverWait(driver4, 20)
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.tagName('iframe')))

		List<WebElement> iframe1 = WebUiCommonHelper.findWebElements(obj2, 2)
		driver4.switchTo().frame(iframe1.get(1))

		WebElement item1 = driver4.findElement(By.xpath('//*[@id="tinymce"]'))
		String desc1 = '[indicator id=1]'
		item1.sendKeys(desc1)
		driver4.switchTo().defaultContent()
	}

	@Keyword
	public String IndentificacionAleatoria(){

		int numero = (int) (Math.random() * 100000000) + 1;
	}


	@Keyword
	public String randomString(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("A Registro de Prueba 1");
		palabras.add("A Registro de Prueba 2");
		palabras.add("A Registro de Prueba 3");
		palabras.add("A Registro de Prueba 4");
		palabras.add("A Registro de Prueba 5");
		palabras.add("A Registro de prueba 6");
		palabras.add("A Registro de Prueba 7");
		palabras.add("A Registro de Prueba 8");
		palabras.add("A Registro de Prueba 9");
		palabras.add("A Registro de prueba 1 0");
		palabras.add("A Registro de Prueba 1 1");
		palabras.add("A Registro de Prueba 1 2");
		palabras.add("A Registro de Prueba 1 3");
		palabras.add("A Registro de Prueba 1 4");
		palabras.add("A Registro de Prueba 1 5");
		palabras.add("A Registro de prueba 1 6");
		palabras.add("A Registro de Prueba 1 7");
		palabras.add("A Registro de Prueba 1 8");
		palabras.add("A Registro de Prueba 1 9");
		palabras.add("A Registro de Prueba 2 0");
		palabras.add("A Registro de Prueba 2 1");
		palabras.add("A Registro de Prueba 2 2");
		palabras.add("A Registro de Prueba 2 3");
		palabras.add("A Registro de Prueba 2 4");
		palabras.add("A Registro de Prueba 2 5");
		palabras.add("A Registro de Prueba 2 6");
		palabras.add("A Registro de Prueba 2 7");
		palabras.add("A Registro de Prueba 2 8");
		palabras.add("A Registro de Prueba 2 9");
		palabras.add("A Registro de Prueba 3 0");

		int numero = (int) (Math.random() * 29) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}

	@Keyword
	public String randomStringEdit(){
		List<String> palabrasEdit = new ArrayList<String>();
		palabrasEdit.add("A Edicion de un Registro 1");
		palabrasEdit.add("A Edicion de un Registro 2");
		palabrasEdit.add("A Edicion de un Registro 3");
		palabrasEdit.add("A Edicion de un Registro 4");
		palabrasEdit.add("A Edicion de un Registro 5");
		palabrasEdit.add("A Edicion de un Registro 6");
		palabrasEdit.add("A Edicion de un Registro 7");
		palabrasEdit.add("A Edicion de un Registro 8");
		palabrasEdit.add("A Edicion de un Registro 9");
		palabrasEdit.add("A Edicion de un Registro 1 0");
		palabrasEdit.add("A Edicion de un Registro 1 1");
		palabrasEdit.add("A Edicion de un Registro 1 2");
		palabrasEdit.add("A Edicion de un Registro 1 3");
		palabrasEdit.add("A Edicion de un Registro 1 4");
		palabrasEdit.add("A Edicion de un Registro 1 5");
		palabrasEdit.add("A Edicion de un Registro 1 6");
		palabrasEdit.add("A Edicion de un Registro 1 7");
		palabrasEdit.add("A Edicion de un Registro 1 8");
		palabrasEdit.add("A Edicion de un Registro 1 9");
		palabrasEdit.add("A Edicion de un Registro 2 0");
		palabrasEdit.add("A Edicion de un Registro 2 1");
		palabrasEdit.add("A Edicion de un Registro 2 2");
		palabrasEdit.add("A Edicion de un Registro 2 3");

		int numero = (int) (Math.random() * 22) + 1 ;
		return palabrasEdit.get(numero)
	}

	@Keyword
	public String randomStringOBSOLETO(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("A Registro Obsoleto 1");
		palabras.add("A Registro Obsoleto 2");
		palabras.add("A Registro Obsoleto 3");
		palabras.add("A Registro Obsoleto 4");
		palabras.add("A Registro Obsoleto 5");
		palabras.add("A Registro Obsoleto 6");
		palabras.add("A Registro Obsoleto 7");
		palabras.add("A Registro Obsoleto 8");
		palabras.add("A Registro Obsoleto 9");
		palabras.add("A Registro Obsoleto 1 0");
		palabras.add("A Registro Obsoleto 1 1");
		palabras.add("A Registro Obsoleto 1 2");
		palabras.add("A Registro Obsoleto 1 3");
		palabras.add("A Registro Obsoleto 1 4");
		palabras.add("A Registro Obsoleto 1 5");
		palabras.add("A Registro Obsoleto 1 6");
		palabras.add("A Registro Obsoleto 1 7");
		palabras.add("A Registro Obsoleto 1 8");
		palabras.add("A Registro Obsoleto 1 9");
		palabras.add("A Registro Obsoleto 2 0");
		palabras.add("A Registro Obsoleto 2 1");
		palabras.add("A Registro Obsoleto 2 2");
		palabras.add("A Registro Obsoleto 2 3");

		int numero = (int) (Math.random() * 22) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}

	@Keyword
	public String randomStringSinObligatorios(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("Registro Sin Obligatorios 1");
		palabras.add("Registro Sin Obligatorios 2");
		palabras.add("Registro Sin Obligatorios 3");
		palabras.add("Registro Sin Obligatorios 4");
		palabras.add("Registro Sin Obligatorios 5");
		palabras.add("Registro Sin Obligatorios 6");
		palabras.add("Registro Sin Obligatorios 7");
		palabras.add("Registro Sin Obligatorios 8");
		palabras.add("Registro Sin Obligatorios 9");
		palabras.add("Registro Sin Obligatorios 1 0");
		palabras.add("Registro Sin Obligatorios 1 1");
		palabras.add("Registro Sin Obligatorios 1 2");
		palabras.add("Registro Sin Obligatorios 1 3");
		palabras.add("Registro Sin Obligatorios 1 4");
		palabras.add("Registro Sin Obligatorios 1 5");
		palabras.add("Registro Sin Obligatorios 1 6");
		palabras.add("Registro Sin Obligatorios 1 7");
		palabras.add("Registro Sin Obligatorios 1 8");
		palabras.add("Registro Sin Obligatorios 1 9");
		palabras.add("Registro Sin Obligatorios 2 0");
		palabras.add("Registro Sin Obligatorios 2 1");
		palabras.add("Registro Sin Obligatorios 2 2");
		palabras.add("Registro Sin Obligatorios 2 3");

		int numero = (int) (Math.random() * 22) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}

	@Keyword
	public String randomStringSinCampos(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("Registro Sin Campos 1");
		palabras.add("Registro Sin Campos 2");
		palabras.add("Registro Sin Campos 3");
		palabras.add("Registro Sin Campos 4");
		palabras.add("Registro Sin Campos 5");
		palabras.add("Registro Sin Campos 6");
		palabras.add("Registro Sin Campos 7");
		palabras.add("Registro Sin Campos 8");
		palabras.add("Registro Sin Campos 9");
		palabras.add("Registro Sin Campos 1 0");
		palabras.add("Registro Sin Campos 1 1");
		palabras.add("Registro Sin Campos 1 2");
		palabras.add("Registro Sin Campos 1 3");
		palabras.add("Registro Sin Campos 1 4");
		palabras.add("Registro Sin Campos 1 5");
		palabras.add("Registro Sin Campos 1 6");
		palabras.add("Registro Sin Campos 1 7");
		palabras.add("Registro Sin Campos 1 8");
		palabras.add("Registro Sin Campos 1 9");
		palabras.add("Registro Sin Campos 2 0");
		palabras.add("Registro Sin Campos 2 1");
		palabras.add("Registro Sin Campos 2 2");
		palabras.add("Registro Sin Campos 2 3");

		int numero = (int) (Math.random() * 22) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}

	@Keyword
	public String randomStringSinNotaRequerida(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("Registro Sin Nota Requerida 1");
		palabras.add("Registro Sin Nota Requerida 2");
		palabras.add("Registro Sin Nota Requerida 3");
		palabras.add("Registro Sin Nota Requerida 4");
		palabras.add("Registro Sin Nota Requerida 5");
		palabras.add("Registro Sin Nota Requerida 6");
		palabras.add("Registro Sin Nota Requerida 7");
		palabras.add("Registro Sin Nota Requerida 8");
		palabras.add("Registro Sin Nota Requerida 9");
		palabras.add("Registro Sin Nota Requerida 1 0");
		palabras.add("Registro Sin Nota Requerida 1 1");
		palabras.add("Registro Sin Nota Requerida 1 2");
		palabras.add("Registro Sin Nota Requerida 1 3");
		palabras.add("Registro Sin Nota Requerida 1 4");
		palabras.add("Registro Sin Nota Requerida 1 5");
		palabras.add("Registro Sin Nota Requerida 1 6");
		palabras.add("Registro Sin Nota Requerida 1 7");
		palabras.add("Registro Sin Nota Requerida 1 8");
		palabras.add("Registro Sin Nota Requerida 1 9");
		palabras.add("Registro Sin Nota Requerida 2 0");
		palabras.add("Registro Sin Nota Requerida 2 1");
		palabras.add("Registro Sin Nota Requerida 2 2");
		palabras.add("Registro Sin Nota Requerida 2 3");

		int numero = (int) (Math.random() * 22) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}
	@Keyword
	public String randomStringSinCamposSinInfo(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("Registro Sin Campos Sin Info 1");
		palabras.add("Registro Sin Campos Sin Info 2");
		palabras.add("Registro Sin Campos Sin Info 3");
		palabras.add("Registro Sin Campos Sin Info 4");
		palabras.add("Registro Sin Campos Sin Info 5");
		palabras.add("Registro Sin Campos Sin Info 6");
		palabras.add("Registro Sin Campos Sin Info 7");
		palabras.add("Registro Sin Campos Sin Info 8");
		palabras.add("Registro Sin Campos Sin Info 9");
		palabras.add("Registro Sin Campos Sin Info 1 0");
		palabras.add("Registro Sin Campos Sin Info 1 1");
		palabras.add("Registro Sin Campos Sin Info 1 2");
		palabras.add("Registro Sin Campos Sin Info 1 3");
		palabras.add("Registro Sin Campos Sin Info 1 4");
		palabras.add("Registro Sin Campos Sin Info 1 5");
		palabras.add("Registro Sin Campos Sin Info 1 6");
		palabras.add("Registro Sin Campos Sin Info 1 7");
		palabras.add("Registro Sin Campos Sin Info 1 8");
		palabras.add("Registro Sin Campos Sin Info  1 9");
		palabras.add("Registro Sin Campos Sin Info 2 0");
		palabras.add("Registro Sin Campos Sin Info 2 1");
		palabras.add("Registro Sin Campos Sin Info 2 2");
		palabras.add("Registro Sin Campos Sin Info 2 3");

		int numero = (int) (Math.random() * 22) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}

	@Keyword
	public String randomStringMetaVariable(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("A Registro MetaVariable 1");
		palabras.add("A Registro MetaVariable 2");
		palabras.add("A Registro MetaVariable 3");
		palabras.add("A Registro MetaVariable 4");
		palabras.add("A Registro MetaVariable 5");
		palabras.add("A Registro MetaVariable 6");
		palabras.add("A Registro MetaVariable 7");
		palabras.add("A Registro MetaVariable 8");
		palabras.add("A Registro MetaVariable 9");
		palabras.add("A Registro MetaVariable 1 0");
		palabras.add("A Registro MetaVariable 1 1");
		palabras.add("A Registro MetaVariable 1 2");
		palabras.add("A Registro MetaVariable 1 3");
		palabras.add("A Registro MetaVariable 1 4");
		palabras.add("A Registro MetaVariable 1 5");
		palabras.add("A Registro MetaVariable 1 6");
		palabras.add("A Registro MetaVariable 1 7");
		palabras.add("A Registro MetaVariable 1 8");
		palabras.add("A Registro MetaVariable 1 9");
		palabras.add("A Registro MetaVariable 2 0");
		palabras.add("A Registro MetaVariable 2 1");
		palabras.add("A Registro MetaVariable 2 2");
		palabras.add("A Registro MetaVariable 2 3");

		int numero = (int) (Math.random() * 22) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}

	@Keyword
	public String randomStringDemyNum(){
		//Se debe de cambiar el nombre del metodo por algo mas especifico, como: inputCreateSettingsIndicator o algo asi. En Ingles
		List<String> palabras = new ArrayList<String>();
		palabras.add("A Registro Dem Num 1");
		palabras.add("A Registro Dem Num 2");
		palabras.add("A Registro Dem Num 3");
		palabras.add("A Registro Dem Num 4");
		palabras.add("A Registro Dem Num 5");
		palabras.add("A Registro Dem Num 6");
		palabras.add("A Registro Dem Num 7");
		palabras.add("A Registro Dem Num 8");
		palabras.add("A Registro Dem Num 9");
		palabras.add("A Registro Dem Num 1 0");
		palabras.add("A Registro Dem Num 1 1");
		palabras.add("A Registro Dem Num 1 2");
		palabras.add("A Registro Dem Num 1 3");
		palabras.add("A Registro Dem Num 1 4");
		palabras.add("A Registro Dem Num 1 5");
		palabras.add("A Registro Dem Num 1 6");
		palabras.add("A Registro Dem Num 1 7");
		palabras.add("A Registro Dem Num 1 8");
		palabras.add("A Registro Dem Num 1 9");
		palabras.add("A Registro Dem Num 2 0");
		palabras.add("A Registro Dem Num 2 1");
		palabras.add("A Registro Dem Num 2 2");
		palabras.add("A Registro Dem Num 2 3");

		int numero = (int) (Math.random() * 22) + 1;
		WebUI.comment(palabras.get(numero))
		return palabras.get(numero)
	}

	@Keyword
	public void uploadFile (String to, String filePath) {
		WebUI.executeJavaScript(to, null)
		StringSelection ss = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		WebUI.delay(5)
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}


	//Revisar en donde se llama este metodo y para que la validacion...
	@Keyword
	public void display_list_only(TestObject list, int num){
		List<WebElement> list_disp = WebUiCommonHelper.findWebElements(list, 5)
		WebUI.comment('Numero es: ' + list_disp.size())

		WebElement list_selected = list_disp.get(num)
		WebUI.delay(5)
		list_selected.click()


		WebUI.delay(5)
	}

	@Keyword
	public void display_list_multi(TestObject list, int num){
		List<WebElement> list_disp = WebUiCommonHelper.findWebElements(list, 5)
		WebUI.comment('Numero de Span con Role="Presentation" para seleccion es: ' + list_disp.size())

		WebElement list_selected = list_disp.get(num)
		WebUI.delay(5)
		list_selected.click()

		WebUI.delay(5)

	}

	@Keyword
	public void list_Action(TestObject obj, int index){

		WebDriver driver = DriverFactory.getWebDriver()
		Actions action = new Actions(driver)

		WebUI.delay(3)

		List<WebElement> list_input = WebUiCommonHelper.findWebElements(obj, 5)
		/*Esta lista retorna todos los elementos input que hay en el formulario, en total son*/
		WebUI.comment('Numero de Input' + list_input.size())

		WebUI.delay(3)

		for (int i=0; i < list_input.size(); i++){
			WebUI.comment('Elemento #: ' + i + ' en la posicion: ' + list_input.get(i))
		}

		WebElement element = list_input.get(index)
		action.doubleClick(element).perform();
	}

	@Keyword
	public void element_selected(TestObject ele, int num){

		WebUI.delay(3)

		List<WebElement> elementos = WebUiCommonHelper.findWebElements(ele, 5)
		WebUI.comment('Numero de Elementos: ' + elementos.size())

		WebUI.delay(3)

		WebElement element = elementos.get(num)
		element.click()

	}


	/*Este metodo es el encargado de seleccionar el ultimo item existente en la lista
	 de algun modulo en cuestion...Esto es aplicable a cualquier lista de cualquier modulo...*/
	@Keyword
	public int lista(TestObject obj){

		WebUI.delay(3)

		//Aqui obtenemos la lista de todos los inputCheck que hay en la pagina..
		List<WebElement> list_items = WebUiCommonHelper.findWebElements(obj, 5)
		WebUI.comment('Numero de Elementos' + list_items.size())

		//En este punto obtenemos el valor del id del ultimo item creado, es decir que este sera
		//el ultimo elemento creado...
		int max = 0
		int valor = 0
		for (int i = 0; i < list_items.size(); i++) {
			WebUI.comment('Dele a eso...' + list_items.get(i))
			valor = Integer.valueOf(list_items.get(i).getAttribute("value"))
			if (valor > max) {
				max = valor;
			}
		}

		//Por ultimo obtenemos el Elemento Web que es el ultimo creado para proceder a dar un click()
		String val = ""
		int apo = 0
		WebElement con

		for (int x=0; x < list_items.size();x++){
			val = list_items.get(x).getAttribute("value")
			if(Integer.parseInt(val) == max){
				con = list_items.get(x)
			}
		}



		WebUI.comment('Impresion de Variable con: ' + con.getAttribute("value"))
		WebUI.comment('Impresion de Variable apo: ' + apo)

		WebUI.comment('Max: ' + max)
		String cadena = Integer.toString(max)
		WebUI.comment('Max String: ' + cadena)

		con.click()


		return max;


	}

	@Keyword
	public void select_item_managment(TestObject lista_href, int id){

		//TestObject obj = WebUI.modifyObjectProperty(lista_href, 'href', 'equals', '/app.php/staff/actionplan/${id}', false);

		List<WebElement> list = WebUiCommonHelper.findWebElements(lista_href, 5)
		WebUI.comment('Numero de Elementos: ' + list.size())

		WebElement item;

		for(int i; i < list.size(); i++){
			if(list.get(i).getText() == id.toString()){
				item = list.get(i)
			}else{
				WebUI.comment('No lo encuentra prro...')
			}
		}

		WebUI.comment('Texto: '+ item.getText())
		WebUI.comment('Texto: '+ item.getAttribute("href"))
		WebUI.comment('Texto: '+ item.getAttribute("data-original-title"))
		item.click()


	}

	@Keyword
	public int select2_config(TestObject obj){

		WebElement dropdown = WebUiCommonHelper.findWebElement(obj, 5);
		Select select = new Select(dropdown)
		List<WebElement> options = select.getOptions();

		int valor = 0;
		int max = 0;

		for(int i=0; i < options.size(); i++){
			WebUI.comment('Elementos ' + options.get(i).getAttribute("value"));
			if(options.get(i).getAttribute("value") == ""){
				valor = 1;
			}else{
				valor = Integer.parseInt((options.get(i).getAttribute("value")));
				if(valor > max){
					max = valor;
				}
			}
		}


		return max;
	}

	@Keyword
	public void ValidarFiltros(String abrr,TestObject abr){

		String valorTipo2 = ""

		List<WebElement> listAbrr = WebUiCommonHelper.findWebElements(abr, 5)
		WebUI.comment('Elementos Existentes: ' + listAbrr.size())

		for (int i = 0; i < listAbrr.size(); i++){
			valorTipo2 = listAbrr.get(i).getText()
			//valorTipo2.replace(" ", "")
			if (valorTipo2 == abrr) {
				WebUI.comment('Son Iguales, el filtro es correcto')
				WebUI.comment(valorTipo2)
			}
		}
	}



	@Keyword
	public boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}


	@Keyword
	public int list_report(TestObject obj){

		List<WebElement> list = WebUiCommonHelper.findWebElements(obj, 5)
		WebUI.comment('Cantidad= ' + list.size())

		int max = 0

		int valor = 0
		for (int i = 0; i < list.size(); i++) {
			if (isNumeric(list.get(i).getText()) == true) {
				valor = Integer.parseInt(list.get(i).getText());
				System.out.println("Numero: " + list.get(i).getText());
			} else {
				System.out.println("No es un numero");
			}
			WebUI.comment('Dele a eso...' + list.get(i))
			if (valor > max) {
				max = valor;
			}
		}
		WebUI.comment('Valor del ultimo "id": '+ max)
		WebUI.delay(5)

		return max;
	}

	@Keyword
	public void validation(TestObject obj, int val){

		List<WebElement> report = WebUiCommonHelper.findWebElements(obj, 5)
		WebUI.comment('Cantidad= ' + report.size())



		int valor2 = 0
		for(int x = 0; x < report.size(); x++){
			if (isNumeric(report.get(x).getText()) == true) {
				valor2 = Integer.parseInt(report.get(x).getText());
				//System.out.println("Numero: " + report.get(x).getText());
			} else {
				System.out.println("No es un numero");
			}
			//WebUI.comment('Contenido del Elemento' + report.get(x).getText())
			//WebUI.comment('Valor de Max: '+ val)
			if (valor2 == val){
				WebUI.comment('Son iguales...')
			}else {
				WebUI.comment('Diferentes')
			}
		}
	}

	@Keyword
	public void select2_multiselect(TestObject obj, int num) {

		List<WebElement> list_items = WebUiCommonHelper.findWebElements(obj, 5);
		WebUI.comment('Cantidad de Items: ' + list_items.size());

		WebElement item = list_items.get(num);
		item.click();

	}

	@Keyword
	public String get_numbers(TestObject obj){

		WebElement elem = WebUiCommonHelper.findWebElement(obj, 5)
		String cadena = elem.getText()

		char [] cadena_div = cadena.toCharArray();
		String n = "";

		for(int i=0; i < cadena_div.length; i++){
			if(Character.isDigit(cadena_div[i])){
				n+=cadena_div[i]
			}
		}
		return n;
	}

	public String get_numbers1(TestObject obj){

		WebElement elem = WebUiCommonHelper.findWebElement(obj, 5)
		String cadena = elem.getText()

		return cadena;
	}

	@Keyword
	public void select_items_actions(String css){

		WebDriver driver = DriverFactory.getWebDriver()
		//Si el selector cambia, cambiar esta linea...
		WebElement elemento = driver.findElement(By.cssSelector(css))
		elemento.click()

	}

	@Keyword
	public void selectItem_taskState(TestObject obj){

		List<WebElement> list = WebUiCommonHelper.findWebElements(obj, 5)
		WebUI.comment('Numero de Items: ' + list.size())

		for(int i=0; i < list.size(); i++){
			WebElement check = list.get(i)
			check.click()
		}

	}


}