<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>8.Tipos Clientes_SAC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f16dcd73-25a8-4481-aab3-e9c1d4181c43</testSuiteGuid>
   <testCaseLink>
      <guid>acc2e768-caa7-4cf1-81bc-3974ddac8955</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35182c85-d538-404f-84bc-56d2a94638a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc9360ec-5059-4e0b-b2c4-80885de08408</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49005209-8817-4abf-a839-aef6163d26ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1f8398f-73f8-45b4-9d46-c9de65a1a9e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>428a0e8e-e062-4c69-92ac-4c44274b41b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>808bed74-ea02-4fab-94d0-e556cfa7ba58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/6.Desfiltrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4276c17f-9afa-4249-8cfa-7ce1d8ba4739</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/8.Tipos de clientes/1.Test Case Generales/7.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
