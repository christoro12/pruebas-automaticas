<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>10.Tipos de Encuestas_SAC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4ab9a6d5-f560-4126-8277-55b027596c84</testSuiteGuid>
   <testCaseLink>
      <guid>19bd4fe6-edac-4e49-bd41-3732b72b7235</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ea775aa-beba-4261-9bf1-c24ccbb5f511</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd70d11f-20d5-460d-bb11-94c82aaa03c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acfcc837-7f38-4b8b-b7f8-a11bef060aaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8a6764c-17fa-45f3-8f62-cba641f4ffee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da77b24c-54e3-41e3-8cf8-1eeb29c00073</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>befc2fe8-ac27-426e-97c4-b3fc1de609f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/6.Desfiltrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fc95cb3-7aad-4759-bb75-9b9b8d865e95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/10.Tipos de encuestas/1.Test Case Generales/7.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
