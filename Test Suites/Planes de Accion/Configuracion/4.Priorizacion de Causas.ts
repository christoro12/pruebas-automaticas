<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>4.Priorizacion de Causas</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>jsandoval@tiqal.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a97dc0e7-4d2a-4df5-ae83-c280f32271dc</testSuiteGuid>
   <testCaseLink>
      <guid>92b672f7-b2e6-4cbb-9a10-461ce02d4f9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73dc9f37-f9c1-47d8-86da-201a483bb316</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f96ed005-61d3-4b91-839d-7fc0fdf999c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30780092-101a-4199-840f-eb0cb488a73a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fd376cd-e9ed-4980-884a-a22957f5310c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>956d0ccf-f40e-4c53-9cf4-311dd0f504aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/5.Filtrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
