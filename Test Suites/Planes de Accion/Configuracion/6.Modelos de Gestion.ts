<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>6.Modelos de Gestion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7967c3f5-14f1-47d4-a503-3db4db47099c</testSuiteGuid>
   <testCaseLink>
      <guid>b3d9d38f-f2e4-4e6c-bef4-0a0c4f4a590b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Test Cases General/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e1c276d-6302-449e-89d2-5178a3e47673</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Test Cases General/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08397f08-8035-4e19-85ee-d876141aa70b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Test Cases General/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e047647f-5e6e-49c2-a420-21441ba4205f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Test Cases General/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2428e40e-4530-42e0-a46d-4bec49d6b3b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Test Cases General/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>445fb2bb-c6ae-4333-8f90-ca01a4e4e2bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Test Cases General/5.Filtrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
