<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1.Tipos</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>88d62692-abf5-4581-ae4e-1aa33f8f02c6</testSuiteGuid>
   <testCaseLink>
      <guid>9e985f61-5f70-4ab1-ad26-3e110f955c7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c025a0e-045b-4b2a-8d26-2b8d348fdf09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e37afb9b-9e7c-42b2-ac42-633d3bf5989e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>637a46d8-edf0-4555-be52-69f4b07f4579</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48723514-a65f-46a6-82d6-2a11f4266187</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0458c58-050c-451f-b08a-aff1b06211b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf03acec-f862-4ace-b919-217a8b264a0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/6.Desfiltrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
