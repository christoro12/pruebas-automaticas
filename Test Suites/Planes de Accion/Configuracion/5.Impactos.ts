<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>5.Impactos</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>jsandoval@tiqal.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4cac0fb0-d6d0-4e84-a4db-89d7a650e7e7</testSuiteGuid>
   <testCaseLink>
      <guid>9d7b334d-e9d4-4e33-9cd8-bef7c22c75af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/5.Impactos/1.Test Cases General/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da2115c1-15ef-4f4b-90b1-13dfbd4255b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/5.Impactos/1.Test Cases General/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36e7e1c2-8345-4c6f-84e2-6f0b374d18e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/5.Impactos/1.Test Cases General/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87e18155-af79-492a-9dd2-465d897339bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/5.Impactos/1.Test Cases General/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c42448aa-e0ce-4dc4-b08a-e38af0fb623a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/5.Impactos/1.Test Cases General/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fce32e7e-eb69-4b5e-961e-5cea6ca1e71e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/5.Impactos/1.Test Cases General/5.Filtrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
