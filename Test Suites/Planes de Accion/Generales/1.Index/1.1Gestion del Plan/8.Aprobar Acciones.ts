<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>8.Aprobar Acciones</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>df9f9fd4-9fcb-43ce-9b02-c3e2b4970e02</testSuiteGuid>
   <testCaseLink>
      <guid>0c3c56e2-38af-4b23-88bb-6ec557a0e702</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.Crear Plan de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f3c74e0-f833-4b15-8bf0-3c5844585120</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/2.Agregar Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3b34c4a-ae6a-445e-8624-0c0b203c9e02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/6.Solicitar aprobacion del plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33de512c-83c9-4f91-978b-e23684bf6aac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/7.Ejecutar Acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5e0aff9-1895-45ad-a375-a1bbfc73aad7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/8.Aprobar acciones</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
