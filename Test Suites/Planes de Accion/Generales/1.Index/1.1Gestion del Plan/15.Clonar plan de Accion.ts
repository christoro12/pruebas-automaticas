<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>15.Clonar plan de Accion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>bd0916f7-2e4b-4962-b620-3635928e642c</testSuiteGuid>
   <testCaseLink>
      <guid>54966e12-ff38-406f-830f-51c130e0e625</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.Crear Plan de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96ed0b31-25a0-471f-ba9a-9c6a7df93f4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/2.Agregar Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e55c719-a7e0-403d-991d-870889707430</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/6.Solicitar aprobacion del plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bc56901-2719-49c1-85d8-3add64ec29a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/7.Ejecutar Acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f6494af-395f-496c-b25f-43c725819fd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/8.Aprobar acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3779fc7-72dc-4188-afb8-053e24c3f609</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/16.Seguir Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00205f45-484c-4f39-9ba9-a566f021ba29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/17.Concluir Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe21aa3c-01c2-4197-914e-42b18787a4ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/14.Seguimiento Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>295440a4-8502-421d-ae77-fcb0f3bf0e65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/15.Clonar Plan Accion</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
