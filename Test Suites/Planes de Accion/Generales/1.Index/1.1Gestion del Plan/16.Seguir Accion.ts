<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>16.Seguir Accion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>25c19469-cc86-4ea1-b2c0-884d41666661</testSuiteGuid>
   <testCaseLink>
      <guid>8c29b5e7-7563-4d37-a993-933bb27e3e8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.Crear Plan de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86f2d5f0-5942-4b6d-a39b-8b97efb2c1e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/2.Agregar Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ea722c9-627b-46e8-b3bd-d96284b20558</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/6.Solicitar aprobacion del plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c84d398f-1db1-4e08-8595-9b9a54d0e5ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/7.Ejecutar Acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d7ffc1b-63fe-466b-9351-b675626027ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/8.Aprobar acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebe91b59-3633-4677-b92f-dda0d772b10d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/16.Seguir Accion</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
