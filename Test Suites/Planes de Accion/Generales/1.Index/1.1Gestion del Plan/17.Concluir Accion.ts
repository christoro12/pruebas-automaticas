<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>17.Concluir Accion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aaff8748-837b-4a14-8e45-699b25c1d8c0</testSuiteGuid>
   <testCaseLink>
      <guid>1a4bb18e-7e4d-4a80-8b1a-f2c630353aec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.Crear Plan de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8846f1c6-bbc2-407d-bf6f-eb0e80234bd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/2.Agregar Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>161a0d4e-02bf-49f4-b42c-d1899cd74d26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/6.Solicitar aprobacion del plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7c39f64-30e6-4c60-bee3-7c1615dc4ec9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/7.Ejecutar Acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c8af2a0-e3eb-4162-a6ea-f778f6fe4e3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/8.Aprobar acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0982f89e-074f-4383-925a-d4a51d501721</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/16.Seguir Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1fe77f8-6df3-4c3e-be03-db803d956811</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/17.Concluir Accion</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
