<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>4.clase de riegos</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>599251a5-fd44-46d1-855f-ea053dda1285</testSuiteGuid>
   <testCaseLink>
      <guid>6a084ab5-2825-4c17-9b43-8a508dad3ae5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.clase de riesgos/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cca5c7f-0dc6-477d-8cdc-cd74679afc7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.clase de riesgos/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>109c629c-2bf1-4b45-b7d4-54c8123b76f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.clase de riesgos/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31e9ab43-fadf-4023-b14c-e82248ab6ef4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.clase de riesgos/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df160bbf-e888-494e-af5b-a734d2c7958a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.clase de riesgos/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09f18b63-dde5-4fc4-977f-62230fc67dad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.clase de riesgos/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07ee74ce-3da5-4865-b0a3-02ce04df4f1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.clase de riesgos/7.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
