import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Configuracion/1.Tipos/1.Test Case General/0.Ver'), [:])

WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/3.input_typename'), 'Edicion')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/4.inputCheck_DeselectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/5.inputCheck_DeselectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/6.inputCheck_notes_required'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/7.textarea_Description'), 'Estoy editando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/8.btn_Save'))

WebUI.delay(5)

