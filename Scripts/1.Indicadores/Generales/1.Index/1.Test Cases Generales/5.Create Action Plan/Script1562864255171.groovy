import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebDriverException as WebDriverException
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

String user = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.user'))

WebUI.delay(10)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/12.priorizar incidentes/1.order1'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/12.priorizar incidentes/1.order2'))

WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/1.inputCheck_Item'))
CustomKeywords.'test.Metodos.lista'(findTestObject('1.Indicadores/General/6.Equipos/0.Ver/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/2.btn_indicator_options'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/3.a_create_action_plan'))

//Proceso Origen 
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Area Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 1)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Proyectos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    0)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/1.type'), '1', false)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 0)

WebUI.delay(5)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 1)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/8.select_track_freq'), '3', 
    true)

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/9.input_num_periods'), '2')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/10.select_criticality_id'), 
    '8', true)

WebUI.delay(5)

//WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/8.input_num_periods'), '4')
///WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/9.select_criticality_id'), '21', 
// true)
//Responsables
//Lider
//CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)
//CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)
//Encargado de Aprobar
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Generales/Select2/1.1.display_list_multi'), 3)

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/1.clear'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/1.input_ap'), user)

WebElement enter = WebUiCommonHelper.findWebElement(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/1.input_ap'), 
    2)

enter.sendKeys(Keys.ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.btn_save'))

