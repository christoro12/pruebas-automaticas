import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/1.btn_Create'))

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/2.select_teams_list'), '12', true)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/3.select2_Team'))

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/3.1.select2_Team'))

WebUI.delay(5)

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/4.input_basename'), palabra)

WebUI.comment('Despues de este input hay un TinyMCE, utilizar Javascript para llenarlo o utilizar el metodo de las Keywords')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/5.input_measurement_unit'), '%')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/6.select_measurement_at_month'), '2', true)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/7.select_measurement_at_day'), '22', true)

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/19.inputCheck_uses_num_and_den'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/20.input_numerator_notes'), 'num')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/21.input_denominator_notes'), 'dem')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/28.input_metaMax'), '100')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/29.input_metaNominal'), '90')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/30. input_metaMinima'), '80')

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/23.btn_Save'))

