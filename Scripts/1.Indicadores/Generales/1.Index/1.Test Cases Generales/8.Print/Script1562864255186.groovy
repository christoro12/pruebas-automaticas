import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebDriverException as WebDriverException
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.delay(10)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/12.priorizar incidentes/1.order1'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/12.priorizar incidentes/1.order2'))

WebUI.delay(5)

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('1.Indicadores/General/6.Equipos/0.Ver/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/7.View/2.btn_Indicator_Options'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/8.Print/3.a_Print'))

WebUI.delay(5)

WebUI.switchToWindowUrl('https://staging.darumasoftware.com/app.php/staff/indicator/print/id/103')

WebUI.delay(5)

