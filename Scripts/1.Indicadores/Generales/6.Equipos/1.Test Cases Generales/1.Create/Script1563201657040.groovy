import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('1.Indicadores/Generales/6.Equipos/1.Test Cases Generales/0.Ver'), [:])

WebUI.click(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/2.input_teamname'), palabra)

WebUI.setText(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/3.input_teamabbr'), 'CLD')

WebUI.setText(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/4.textarea_teamnotes'), 'Esto es una descripcion...')

//Aqui hay que desplegar las dos listas y seleccionar un item de ellas
//Usuario..
WebUI.click(findTestObject('Object Repository/1.Indicadores/General/6.Equipos/1.Crear/5.select2_Team'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/5.1.select2_Team'),1)

WebUI.delay(5)

CustomKeywords.'test.Metodos.display_list_only'(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/5.select2_Team'), 1)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/5.1.select2_Team'),0)

WebUI.click(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/7.btn_addmember'),3)

WebUI.delay(5)

CustomKeywords.'test.Metodos.display_list'(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/5.select2_Team'), 2)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/5.1.select2_Team'),2)

WebUI.delay(5)

CustomKeywords.'test.Metodos.display_list'(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/5.select2_Team'), 3)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/5.1.select2_Team'),3)

WebUI.click(findTestObject('1.Indicadores/General/6.Equipos/1.Crear/8.btn_Save'))

