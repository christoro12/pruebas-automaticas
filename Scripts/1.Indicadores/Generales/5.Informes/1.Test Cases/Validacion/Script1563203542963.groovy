import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

int valor = CustomKeywords.'test.Metodos.list_report'(findTestObject('Object Repository/1.Indicadores/General/5.Informes/1.Validacion/3.indicator_list'))
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/General/5.Informes/0.Ver/1.a_Informes'))

WebUI.click(findTestObject('1.Indicadores/General/5.Informes/1.Validacion/1.a_Reporte general de indicadores'))

WebUI.delay(3)
WebUI.switchToWindowUrl('http://34.210.132.114/app.php/staff/indicator/viewReportByGeneral')
WebUI.delay(5)

CustomKeywords.'test.Metodos.validation'(findTestObject('Object Repository/1.Indicadores/General/5.Informes/1.Validacion/2.id_list'),valor)
