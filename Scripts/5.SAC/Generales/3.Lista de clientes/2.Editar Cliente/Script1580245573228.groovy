import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('5.SAC/Configuracion/1.Tipos de opinion/1.Test Case Generales/0.Ver'), [:])

WebUI.delay(5)

WebUI.click(findTestObject('5.SAC/Generales/3.Lista de clientes/1.Ver/1.List_Customer'))

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('5.SAC/Generales/3.Lista de clientes/1.Editar Cliente Validacion/2.Edit'))

WebUI.delay(5)

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/1.nit'), palabra)

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/2.Direccion'), 'direccion_prueba')

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/3.Barrio'), 'Barrio_prueba')

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/4.Lugar'), 'Lugar_prueba')

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/5.Telefono'), 'Telefono_prueba')

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/6.Email'), 'Email_prueba')

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/7.Contacto_comercial'), 'ContactoComercial_prueba')

WebUI.setText(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/8.Contacto_calidad'), 'ContactoCalidad_prueba')

int element = CustomKeywords.'test.Metodos.select2_config'(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/9.Select'))

WebUI.selectOptionByValue(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/9.Select'), element.toString(), 
    false)

WebUI.click(findTestObject('5.SAC/Generales/3.Lista de clientes/2.Editar Cliente/3.Save'))

