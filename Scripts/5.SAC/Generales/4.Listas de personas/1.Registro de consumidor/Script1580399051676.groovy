import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


String palabra = CustomKeywords.'test.Metodos.randomString'()

String Numero = CustomKeywords.'test.Metodos.IndentificacionAleatoria'()

WebUI.callTestCase(findTestCase('5.SAC/Generales/4.Listas de personas/0.Ver'), [:])

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Create'))

WebUI.delay(3)

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/2.List_customers'))

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Name'), palabra)

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Cedula'), Numero)

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Phone_1'), 'telefono prueba1')

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Phone_2'), 'telefono prueba2')

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Direccion'), 'direccion prueba')

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Barrio'), 'barrio prueba')

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Email'), 'email porueba')

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Place'), 'lugar')

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/1.Registro Consumidor/1.Save'))

WebUI.delay(5)
