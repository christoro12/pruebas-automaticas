import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

//Ver Tipos
WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/0.Ver/2.a_Types'))

//Crear Tipos

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/1.Crear/2.input_typename'), palabra)

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/1.Crear/3.inputCheck_selectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/1.Crear/3.inputCheck_selectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/1.Crear/4.inputCheck_notes_required'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/1.Crear/5.textarea_Description'), 'Esto es una prueba par Katalon.')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/1.Crear/6.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Tipo'))

//Editar Tipo
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/1.inputCheck_items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/3.input_typename'), 'Edicion')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/4.inputCheck_DeselectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/5.inputCheck_DeselectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/6.inputCheck_notes_required'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/7.textarea_Description'), 'Estoy editando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/2.Editar/8.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Tipo'))

//Eliminar Tipo
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/3.Eliminar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabilitar Tipo
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/4.Inhabilitar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Alto/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

//Redireccion Clases
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/0.Ver/1.a_Configuration'))

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/0.Ver/2.a_Class'))


//Clases
//Crear Clases
WebUI.delay(3)

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/1.Crear/1.btn_Create'))


WebUI.setText(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/1.Crear/2.input_Classname'), palabra)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/1.Crear/3.textarea_Description'), 'Estamos Creando un registro.')

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/1.Crear/4.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Clases'))

//Editar Clases
WebUI.delay(3)

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/2.Editar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/2.Editar/3.input_classname'), 'Edicion de un Registro')

WebUI.setText(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/2.Editar/4.textarea_Description'), 'Estamos Editando un registro.')

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/2.Editar/5.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Clases'))

//Eliminar Clases
WebUI.delay(3)

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/3.Eliminar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabilitar Clases
WebUI.delay(3)

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/4.Inhabilitar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/2.Clases/1.Alto/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

//Fallas
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/0.Ver/2.a_Fail'))

WebUI.delay(3)

//Crear Fallas
WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/1.Crear/2.input_failname'), palabra)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/1.Crear/3.textarea_Description'), 'Estamos creando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/1.Crear/4.inputCheck_List'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/1.Crear/5.inputCheck_List'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/1.Crear/6.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Fallas'))

//Editar FAllas

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/2.Editar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/2.Editar/3.input_failname'), 'Edicion de la Prueba')

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/2.Editar/4.textarea_Description'), 'Estamos Editando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/2.Editar/5.inputCheck_List'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/2.Editar/6.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Fallas'))

//Eliminar Fallas

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/3.Eliminar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabilitar Fallas

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/4.Inhabilitar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

//Grupos
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/0.Ver/2.a_Group'))

WebUI.delay(3)

//Crear Grupos
WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/1.Crear/2.input_groupname'), palabra)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/1.Crear/3.textarea_Description'), 'Estamos creando un registro real...')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/1.Crear/4.select_Weighing'), '5',
	true)

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/1.Crear/5.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Grupos'))

//Editar Grupos
WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/2.Editar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/2.Editar/3.input_groupname'), 'Editando un registro')

WebUI.setText(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/2.Editar/4.textarea_Description'), 'Estamos Editando un registro')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/2.Editar/5.select_Weighing'), '10',
	true)

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/2.Editar/6.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Grupos'))

//Eliminar GRupos...
WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/3.Eliminar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabilitar Grupos
WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/4.Inhabilitar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/4.Grupos/1.Alto/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

//Categoria
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/0.Ver/1.a_Configurations'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/0.Ver/2.a_Category'))

WebUI.delay(3)

//Crear Categoria
WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/1.Crear/2.input_categoryname'), palabra)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/1.Crear/3.select_Group'), '1',
	true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/1.Crear/4.textarea_Description'), 'Estamos creando un registro...')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/1.Crear/5.select_Weight'), '5',
	true)

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/1.Crear/6.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Categoria'))

//Editar Categoria
WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/2.Editar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/2.Editar/3.input_categoryname'), 'Editando un registro')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/2.Editar/4.select_Group'), '2',
	true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/2.Editar/5.textarea_Description'), 'Estamos Ediatndo un registro...')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/2.Editar/6.select_weight'), '10',
	true)

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/2.Editar/7.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Categoria'))

//Eliminar Categoria
WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/3.Eliminar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabiltar CAtegoria
WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/4.Inhabilitar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/5.Categoria/1.Alto/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

//Roles
WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/0.Ver/2.a_Roles'))

//Crear Roles
WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/1.Crear/2.input_rolename'), palabra)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/1.Crear/3.select_role_type'), '3',
	true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/1.Crear/4.textarea_Description'), 'Esto es una prueba de Katalon')

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/1.Crear/5.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Roles'))

//Editar Roles
WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/2.Editar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/2.Editar/3.input_rolename'), 'Edicion prueba...')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/2.Editar/4.select_role_type'), '4',
	true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/2.Editar/5.textarea_Description'), 'Esto es una edicion de Katalon')

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/2.Editar/6.btn_Save'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Roles'))

//Eliminar Roles
WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/3.Eliminar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabilitar Roles
WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/4.Inhabilitar/1.inputCheck_Items'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Alto/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

