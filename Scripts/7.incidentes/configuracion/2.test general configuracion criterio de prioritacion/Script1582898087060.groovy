import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/0.ver/1.configuracion'))

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/0.ver/1.criterio'))

WebUI.delay(6)

String palabra1 = CustomKeywords.'test.Metodos.randomString'()

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.btn_new'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.text_critrio'), palabra1)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.select'), 'HaEvalRequisite', 
    false)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.text_area'), 
    'esto es una prueba')

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/2.select'), '50', 
    false)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/3.select'), '50', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.btn_save'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_back'))

WebUI.delay(3)

String palabra2 = CustomKeywords.'test.Metodos.randomString'()

WebUI.delay(3)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.btn_editar'))

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.text_criterio'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.text_criterio'), palabra2)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.select'), 'DrmOhsasRecord', 
    false)

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.txt_area'))

WebUI.setText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.txt_area'), 'estamos editando')

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/2.select'), '70', 
    false)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/3.select'), '30', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.btn_save'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_back'))

WebUI.delay(3)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.delay(3)

String palabra3 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/5.filtro/1.nombre'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/5.filtro/1.text_name'), palabra3)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/5.filtro/1.select'), '1', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/6.btn_filter'))

WebUI.delay(10)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.delay(3)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/7.btn_Reset_filter'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/6.mostrar/1.show'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/6.mostrar/1.btn_back'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/4.inhabilitar/1.btn_mas'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/4.inhabilitar/1.btn_inhabilitar'))

WebUI.delay(3)

WebUI.acceptAlert()

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/3.eliminar/1.btn_eliminar'))

WebUI.delay(5)

WebUI.acceptAlert()

WebUI.delay(5)

