import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/0.ver/1.configuracion'))

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/0.ver/1.lugares'))

WebUI.delay(6)

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.btn_nuevo'))

WebUI.setText(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.text_lugar'), palabra)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.text_area'), 'Esto es una descripcion')

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.btn_save'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_back'))

WebUI.delay(5)

String palabra1 = CustomKeywords.'test.Metodos.randomString'()

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/2.editar/1.btn_editar'))

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/1.lugares/2.editar/1.text_lugar'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/1.lugares/2.editar/1.text_lugar'), palabra1)

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/1.lugares/2.editar/1.text_area'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/1.lugares/2.editar/1.text_area'), 'Esto es una descripcion')

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/2.editar/1.btn_save'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_back'))

WebUI.delay(5)

//Filtrar
WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/1.btn_Open_Filter'))

WebUI.setText(findTestObject('7.incidentes/configuracion/1.lugares/5.filtrar/1.text_name'), 'Registro de Prueba')

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/1.lugares/5.filtrar/1.select'), '1', true)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/5.filtrar/1.btn_filter'))

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/1.btn_Open_Filter'))

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/5.filtrar/1.btn_reset'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/6.mostrar/2.ver'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/6.mostrar/1.btn_back'))

WebUI.delay(5)

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/4.Inhabilitar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/4.inhabilitar/1.btn_mas'))

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/4.inhabilitar/1.btn_inhabilitar'))

WebUI.acceptAlert()

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/3.eliminar/1.btn_delete'))

WebUI.acceptAlert()

WebUI.delay(5)

