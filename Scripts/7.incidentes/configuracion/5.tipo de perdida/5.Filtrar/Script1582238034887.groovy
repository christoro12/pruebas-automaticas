import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/configuracion/5.tipo de perdida/0.Ver'), [:])

String palabra1 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/5.tipo de perdida/5.filtrar/1.name'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.filtro'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/5.tipo de perdida/5.filtrar/1.input_name'), palabra1)

WebUI.click(findTestObject('7.incidentes/configuracion/5.tipo de perdida/5.filtrar/1.btn_filter'))

WebUI.delay(8)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.filtro'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.reset'))

