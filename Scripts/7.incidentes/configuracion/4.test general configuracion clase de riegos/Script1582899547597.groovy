import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/0.ver/1.configuracion'))

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/0.ver/1.clases_de_riesgo'))

WebUI.delay(5)

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.crear'))

WebUI.setText(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.input name'), palabra)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.text_area'), palabra)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.btn_save'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.bnt_back'))

WebUI.delay(3)

String palabra1 = CustomKeywords.'test.Metodos.randomStringEdit'()

WebUI.delay(3)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/2.editar/1.editar'))

WebUI.clearText(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.input name'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.input name'), palabra1)

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.text_area'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.text_area'), palabra)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/2.editar/1.btn_save'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/1.crear/1.bnt_back'))

WebUI.delay(3)

String palabra3 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.name'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.filtro'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.input name'), palabra3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.bnt_filter'))

WebUI.delay(8)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.filtro'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/5.filtrar/1.reset'))

WebUI.delay(3)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/6.mostrar/1.mostrar'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/6.mostrar/1.btn_back'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/4.inhabilitar/1.more'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/4.inhabilitar/1.disable'))

WebUI.delay(5)

WebUI.acceptAlert()

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/4.clase de riesgos/3.eliminar/1.eliminar'))

WebUI.delay(5)

WebUI.acceptAlert()

