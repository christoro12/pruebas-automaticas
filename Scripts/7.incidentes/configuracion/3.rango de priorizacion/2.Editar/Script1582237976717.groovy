import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('7.incidentes/configuracion/3.rango de priorizacion/0.Ver'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.order'))

WebUI.delay(3)

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.edit'))

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_name'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_name'), palabra)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.select'), 'ProcessIndicator', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_color'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.color'))

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input _limit_inferior'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input _limit_inferior'), '10')

WebUI.delay(3)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_limit_upper'), '50')

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_save'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_back'))

