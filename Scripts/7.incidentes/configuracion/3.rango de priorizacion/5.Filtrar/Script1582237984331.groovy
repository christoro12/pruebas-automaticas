import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/configuracion/3.rango de priorizacion/0.Ver'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.order'))

WebUI.delay(3)

String palabra1 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.name'))

String palabra2 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.text'))

String palabra3 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.limit_lower'))

String palabra4 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.upper_limit'))

WebUI.delay(3)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_name'), palabra1)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_fuente'), palabra2)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_lower_limit'), palabra3)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_upper_limit'), palabra4)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.select'), '1', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.btn_filter'))

WebUI.delay(3)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.reset'))

