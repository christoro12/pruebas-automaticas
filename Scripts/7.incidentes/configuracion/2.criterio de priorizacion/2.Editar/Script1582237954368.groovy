import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('7.incidentes/configuracion/2.criterio de priorizacion/0.Ver'), [:])

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.btn_editar'))

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.text_criterio'))

WebUI.setText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.text_criterio'), palabra)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.select'), 'DrmOhsasRecord', 
    false)

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.txt_area'))

WebUI.setText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.txt_area'), 'estamos editando')

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/2.select'), '70', 
    false)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/3.select'), '30', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/2.editar/1.btn_save'))

