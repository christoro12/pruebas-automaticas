import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/0.ver/1.configuracion'))

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/0.ver/1.rango_priorizacion'))

WebUI.delay(5)

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.create'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_name'), palabra)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.select'), 'HaEvalRequisite', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_color'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.color'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input _limit_inferior'), '1')

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_limit_upper'), '45')

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.btn_save'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.btn_back'))

WebUI.delay(3)

String palabra5 = CustomKeywords.'test.Metodos.randomString'()

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.order'))

WebUI.delay(3)

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.edit'))

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_name'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_name'), palabra5)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.select'), 'ProcessIndicator', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_color'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.color'))

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input _limit_inferior'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input _limit_inferior'), '10')

WebUI.delay(3)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/1.crear/1.input_limit_upper'), '50')

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_save'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/2.editar/1.btn_back'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.order'))

WebUI.delay(3)

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/6.mostrarr/1.mostrar'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/6.mostrar/1.btn_back'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.order'))

WebUI.delay(3)

String palabra1 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.name'))

String palabra2 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.text'))

String palabra3 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.limit_lower'))

String palabra4 = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.upper_limit'))

WebUI.delay(3)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_name'), palabra1)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_fuente'), palabra2)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_lower_limit'), palabra3)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.input_upper_limit'), palabra4)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.select'), '1', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.btn_filter'))

WebUI.delay(3)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.reset'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.order'))

WebUI.delay(3)

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/4.Inhabilitar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/4.inhabilitar/1.more'))

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/4.inhabilitar/1.disable'))

WebUI.acceptAlert()

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/5.filtrar/1.order'))

WebUI.delay(3)

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Generales/3.Eliminar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/configuracion/3.rengo de priorizacion/3.elimiar/1.delete'))

WebUI.acceptAlert()

WebUI.delay(5)

