import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.callTestCase(findTestCase('7.incidentes/Daruma/Acceso al modulo'), [:], FailureHandling.STOP_ON_FAILURE)

String user = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/general/1.index/1.crear/1.uset'))

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/1.nuevo'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.crear/1.select'), '9', false)

WebUI.comment(user)

WebUI.delay(5)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 0)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.crear/2.select'), '1', false)

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/1.calendario'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/1.fecha'))

WebUI.delay(5)

WebUI.setText(findTestObject('7.incidentes/general/1.index/1.crear/1.input'), user)

WebUI.delay(5)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 1)

WebUI.delay(5)

WebUI.check(findTestObject('7.incidentes/general/1.index/1.crear/1.req_analisis'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Generales/Select2/1.display_list_only'), 0)

WebUI.delay(5)

WebUI.setText(findTestObject('7.incidentes/general/1.index/1.crear/2.input_use'), user)

WebUI.delay(5)

WebElement enter = WebUiCommonHelper.findWebElement(findTestObject('7.incidentes/general/1.index/1.crear/2.input_use'), 
    2)

enter.sendKeys(Keys.ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/3.select'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Generales/Select2/1.display_list_only'), 2)

WebUI.delay(5)

WebUI.setText(findTestObject('7.incidentes/general/1.index/1.crear/3.inputrep'), 'aseguramiento')

WebElement enter1 = WebUiCommonHelper.findWebElement(findTestObject('7.incidentes/general/1.index/1.crear/3.inputrep'), 
    2)

enter1.sendKeys(Keys.ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/1.btn_save'))

