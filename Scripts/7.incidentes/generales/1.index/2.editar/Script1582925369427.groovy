import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/Daruma/Acceso al modulo'), [:], FailureHandling.STOP_ON_FAILURE)

String user = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/general/1.index/1.crear/1.uset'))

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('7.incidentes/general/1.index/2.editar/1.editar'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.crear/1.select'), '10', false)

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/1.calendario'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/1.fecha'))

WebUI.delay(5)

WebUI.setText(findTestObject('7.incidentes/general/1.index/1.crear/1.input'), user)

WebUI.delay(5)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.crear/3.select'), '2', false)

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/2.editar/1.btn_save'))

WebUI.delay(5)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.crear/1.btn_back'))

