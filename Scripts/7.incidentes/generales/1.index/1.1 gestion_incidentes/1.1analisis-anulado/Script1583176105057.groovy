import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
WebUI.callTestCase(findTestCase('7.incidentes/generales/1.index/4.mostrar'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis/1.btn_analisis'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis/1.opc_analisis'))

WebUI.delay(3)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 0)

WebUI.delay(3)

CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Generales/Select2/1.display_list_only'), 0)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis/1.input'), FailureHandling.STOP_ON_FAILURE)

WebElement enter = WebUiCommonHelper.findWebElement(findTestObject('7.incidentes/general/1.index/1.crear/2.input_use'), 
    2)

enter.sendKeys(Keys.ENTER)

WebUI.delay(3)

//WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis/1.select'), '', false)
CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 1)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis/2.select'), '11', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis/1.btn_save'), FailureHandling.STOP_ON_FAILURE)

