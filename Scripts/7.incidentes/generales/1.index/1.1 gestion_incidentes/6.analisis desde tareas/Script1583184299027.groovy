import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/generales/1.index/4.mostrar'), [:], FailureHandling.STOP_ON_FAILURE)

String user = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.user'))

String codigo = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/7.correccion_desde_tareas/1.id'))

WebUI.comment(codigo)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/7.correccion_desde_tareas/1.tareas'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/6.analisis_desde_tareas/1.dezliegue'))

WebUI.delay(3)

CustomKeywords.'test.Metodos.comparar'(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/7.correccion_desde_tareas/1.casos'), 
    codigo)

WebUI.delay(3)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 0)

WebUI.delay(3)

CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Generales/Select2/1.1.display_list_multi'), 0)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis-en_proceso/1.seleccion'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

//WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis/1.select'), '', false)
CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 1)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis-en_proceso/2.select'), 
    '11', false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/1.analisis-en_proceso/1.btn_save'), FailureHandling.STOP_ON_FAILURE)

