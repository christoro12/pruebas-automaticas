import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/generales/1.index/4.mostrar'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/1.btn_correcion'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/1.opc_correcion'))

WebUI.delay(3)

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/1.text area'), palabra)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/1.input_costo'), '182525')

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/1.select'), '', 
    false)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/2.select'), '', 
    false)

WebUI.delay(3)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 0)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/3.select'), '13', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/1btn_save'))

