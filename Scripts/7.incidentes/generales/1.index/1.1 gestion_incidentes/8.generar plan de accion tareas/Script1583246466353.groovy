import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('7.incidentes/generales/1.index/4.mostrar'), [:], FailureHandling.STOP_ON_FAILURE)

String user = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.user'))

String codigo = CustomKeywords.'test.Metodos.get_Text'(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/7.correccion_desde_tareas/1.id'))

WebUI.comment(codigo)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/7.correccion_desde_tareas/1.tareas'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/6.analisis_desde_tareas/1.dezliegue'))

WebUI.delay(3)

CustomKeywords.'test.Metodos.comparar'(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/7.correccion_desde_tareas/1.casos'), 
    codigo)

WebUI.delay(3)

CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)

WebUI.delay(3)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

WebUI.delay(3)

//Area Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 1)

WebUI.delay(3)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.select'), 
    '1', false)

//Add Custom Keyword for TinyMCE Description of the action plan
CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 0)

WebUI.delay(5)

//Add Custom Keyword for TinyMCE Closing goal
CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 1)

WebUI.delay(5)

//Add Custom Keyword for TinyMCE Closing goal
CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe'), 2)

WebUI.delay(5)

//WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/8.input_num_periods'), '4')
///WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/9.select_criticality_id'), '21', 
// true)
//Responsables
//Lider
//CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)
//CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)
//Encargado de Aprobar
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Generales/Select2/1.1.display_list_multi'), 3)

WebUI.delay(2)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.clear'))

WebUI.setText(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.input'), user)

WebElement enter = WebUiCommonHelper.findWebElement(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.input'), 
    2)

enter.sendKeys(Keys.ENTER)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/3.select'), '13', 
    false)

WebUI.delay(2)

WebUI.click(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/3.generar_plan_de_accion/1.btn_save'))

