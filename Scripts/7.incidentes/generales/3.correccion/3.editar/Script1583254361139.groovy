import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('7.incidentes/Daruma/Acceso al modulo'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/3.correccion/1.filtrar correccion/1.correccion'))

WebUI.delay(3)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/3.correccion/2.mostrar especificaciones/1.mostrar'))

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/3.correccion/3.editar/1.btn_editar'))

WebUI.delay(3)

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/general/3.correccion/3.editar/1.text area'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/general/3.correccion/3.editar/1.text area'), palabra)

WebUI.delay(3)

WebUI.clearText(findTestObject('7.incidentes/general/3.correccion/3.editar/1.input costo'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/general/3.correccion/3.editar/1.input costo'), '282825')

WebUI.delay(3)

not_run: WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/1.select'), 
    '', false)

not_run: WebUI.delay(3)

not_run: WebUI.selectOptionByValue(findTestObject('7.incidentes/general/1.index/1.1gestion incidentes/2.correccion/2.select'), 
    '', false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/general/3.correccion/3.editar/1.btn_save'))

