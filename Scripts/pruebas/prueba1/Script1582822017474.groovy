import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Login'), [:])

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/29.aprobar acción desde la sección de tareas/1.Task'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/29.aprobar acción desde la sección de tareas/1.List_task_action_plan'))

WebUI.delay(5)

String dynamicId = 'http://52.11.126.234/app.php/staff/actionplan/trackAction/action_id/283'

String xpath = '//a[@href = "' + dynamicId + '"]'

TestObject to = new TestObject('objectName')

to.addProperty('xpath', ConditionType.CONTAINS, xpath)

WebUI.delay(5)

WebUI.click(to)

