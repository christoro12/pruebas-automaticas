import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/0.Ver/2.a_Impactos'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/2.input_impactname'),
	palabra)

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/3.textarea_impactnotes'),
	'Esto es una Descripcion...')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/4.btn_Save'))

WebUI.delay(5)