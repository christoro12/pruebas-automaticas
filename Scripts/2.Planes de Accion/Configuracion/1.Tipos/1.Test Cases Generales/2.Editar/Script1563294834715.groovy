import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomStringEdit'()
WebUI.callTestCase(findTestCase('2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/0.Ver'), [:])

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/1.list_inputCheck'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/3.input_name'), palabra)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/4.inputCheck_has_impact'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/5.inputCheck_has_analysis'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/6.inputCheck_has_budget'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/7.input_planning_days'), '100')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/8.input_approved_days'), '50')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/9.input_closure_days'), '75')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/10.inputCheck_manual_advance'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/11.inputCheck_all_signatures'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/12.clearAll_disable_fields'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/13.btn_Save'))

WebUI.delay(5)