import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.delay(5)

String user = CustomKeywords.'test.Metodos.get_Text'(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.1.Crear/1.user'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/1.btn_Create'))

WebUI.delay(5)

//Proceso Origen 
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Area Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 1)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Proyectos
//CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
//  0)
//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/3.Origenes'))

///WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/3.inputCheck_origins'))
//Registro
//CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
//  1)
//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.1.select_option_multi'), 1)
//Tipo
int elementSelect = CustomKeywords.'test.Metodos.select2_config'(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/4.Element_select'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/5.select_type'), elementSelect.toString(), 
    true)

WebUI.getNumberOfTotalOption(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/4.Element_select'))

//Add Custom Keyword for TinyMCE Description of the action plan
CustomKeywords.'test.Metodos.tiny1'(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/6.TinyMCE'), 0)

WebUI.delay(5)

//Add Custom Keyword for TinyMCE Closing goal
CustomKeywords.'test.Metodos.tiny1'(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/6.TinyMCE'), 1)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/7.select_track_freq'), '4', true)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.1.Crear/3.select_impac_critic'), '21', true)

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/8.input_num_periods'), '1')

///WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/9.select_criticality_id'), '21', 
// true)
//Responsables
//Lider
//CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)
//CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)
//Encargado de Aprobar
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    2)

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.1.Crear/1.enter'), user)

WebElement enter = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.1.Crear/1.enter'), 
    2)

enter.sendKeys(Keys.ENTER)

WebUI.delay(5)

//Responsable de Seguimiento
//CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),3)
//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'),3)
//Responsable de Seguimiento de Efectividad
//CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),4)
//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)
//Procesos Asociados
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Generales/Select2/1.1.display_list_multi'), 5)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Lista de Areas
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    6)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Sistemas de Gestion
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    7)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Modelos
//CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
//  8)
//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)
WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/10.btn_Save'))

WebUI.delay(5)

