import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.junit.After as After
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

/// CREAR PLAN DE ACCION
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/1.btn_Create'))

WebUI.delay(5)

//Proceso Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 2)

//Area Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 1)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Proyectos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    0)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/2.inputCheck_origins'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/3.inputCheck_origins'))

//Registro
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    1)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.1.select_option_multi'), 1)

//Tipo
int elementSelect = CustomKeywords.'test.Metodos.select2_config'(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/4.Element_select'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/5.select_type'), elementSelect.toString(), 
    true)

//Add Custom Keyword for TinyMCE Description of the action plan
CustomKeywords.'test.Metodos.tiny1'(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/6.TinyMCE'), 0)

WebUI.delay(5)

//Add Custom Keyword for TinyMCE Closing goal
CustomKeywords.'test.Metodos.tiny1'(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/6.TinyMCE'), 1)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/7.select_track_freq'), '3', true)

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/8.input_num_periods'), '1')

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/9.select_criticality_id'), '21', 
    true)

//Responsables
//Lider
//CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)
//CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)
//Encargado de Aprobar
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    2)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Responsable de Seguimiento
//CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),3)
//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'),3)
//Responsable de Seguimiento de Efectividad
//CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),4)
//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)
//Procesos Asociados
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    5)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Lista de Areas
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    6)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Sistemas de Gestion
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    7)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Modelos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    8)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/10.btn_Save'))

WebUI.delay(5)

/// ANALISIS ///
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/1.btn_option_more'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/2.btn_analyze'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/3.btn_Save'))

//Clasificaion
WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/4.select_cause_effect'), 
    '2', false)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/5.inputCheck_add_actions'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/3.btn_Save'))

//Acciones
CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/6.select_action_type'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/7.select_end_date_month'), 
    '12', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/8.select_end_date_day'), 
    '2', true)

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/9.input_initial_budget'), 
    '5000000')

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/10.input_investment_budget'), 
    '5100000')

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/11.input_operating_budget'), 
    '4800000')

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/12.inputCheck_exclude_consolidate'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/3.btn_Save'))

/// SOLICITAR APROBACION DEL PLAN
WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/6.Solicitar Plan de Accion/1.btn_option_more'))

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/6.Solicitar Plan de Accion/2.btn_ask_for_approval'))

WebUI.acceptAlert()

WebUI.delay(5)

/// AGREGAR NOTA 
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/5.Agregar Nota/1.btn_option_more'))

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/5.Agregar Nota/1.btn_newNote'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/5.Agregar Nota/3.btn_Save'))

WebUI.delay(5)

// EJECUTAR ACCIONES
CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > tr > td > a')

//CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > tr:nth-child(6) > td:nth-child(3) > a')
WebUI.delay(5)

CustomKeywords.'test.Metodos.select2_multiselect'(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/1.add_list'), 
    0)

//Cuando se aniade la lista se carga un nombre en el input del nombre de la lista, pero es posible sobreescribirlo. A continuacion queda el Input pero se dejara comentado...
//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Ejecutar Acciones/2.input_task_list_name'), 'Lista para pruebas...')
//Enviar un ENTER.SendKeys
WebElement enter = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/2.input_task_list_name'), 
    5)

enter.sendKeys(Keys.ENTER)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2_multiselect'(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/1.add_list'), 
    1)

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/4.input_task_notes'), 
    'Una tarea...')

WebElement enter2 = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/4.input_task_notes'), 
    5)

enter2.sendKeys(Keys.ENTER)

// APROBAR ACCIONES TAREAS
//WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Test Cases General/1.Return'))
WebUI.delay(8)

//CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > tr > td > a')
//CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > tr:nth-child(7) > td:nth-child(5) > a')
WebUI.delay(5)

CustomKeywords.'test.Metodos.selectItem_taskState'(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/8.Aprobar Acciones/2.inputCheck_task_state'))

/// AGREGAR NOTA AVANCE
WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/5.Agregar Nota/2.btn_newNote'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/5.Agregar Nota/3.btn_Save'))

///WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/8.Aprobar Acciones/3.btn_return_actionPlan'))
///CONCLUIR  TAREAS ACCION
WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/16.Seguir Accion/btn_conclude action'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/16.Seguir Accion/select1'), 
    '100', true)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/5.Agregar Nota/3.btn_Save'))

WebUI.delay(5)

//WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/8.Aprobar Acciones/3.btn_return_actionPlan'))
/// CONCLUIR ACCION
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/17.Concluir Accion/1.Conclude_ActionF'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/17.Concluir Accion/5.Button'))

WebUI.delay(5)

/// script para prueba
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/17.Concluir Accion/2.close'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/14.Seguimiento Accion/1.back_to_plan'))

/// SEGUIR ACCION
WebUI.delay(10)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/14.Seguimiento Accion/a_s1'))

WebUI.delay(10)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Generales/TinyMCE/Iframe2'), 0)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/14.Seguimiento Accion/input2'))

///WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/14.Seguimiento Accion/input_Return2'))
WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/16.Seguir Accion/button_save'))

///CONCLUIR PLAN DE ACCION
WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/1.Conclude_Action'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/2.Input_Objetive_plan'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/3.Efectivo_plan_action'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/4.Textarea'), 
    'prueba katalon')

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/5.Button'))

WebUI.refresh()

