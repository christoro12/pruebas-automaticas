import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.junit.After as After
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), id)

///CONCLUIR PLAN DE ACCION
WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/1.Conclude_Action'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/2.1Input_Objetive_plan_No'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/3.1Efectivo_plan_action_No'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/4.Textarea'), 
    'prueba katalon plan de accion no efectivo, listo para clonar')

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/13.Concluir Plan de Accion/5.Button'))

WebUI.delay(5)

/// SCRIPT SOBRANTE
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/17.Concluir Accion/2.close'))

///////////////////
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/15.Clonar Plan de Accion/input_Return2'))

WebUI.delay(5)

int idi = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), idi)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/15.Clonar Plan de Accion/1.Clone'))

