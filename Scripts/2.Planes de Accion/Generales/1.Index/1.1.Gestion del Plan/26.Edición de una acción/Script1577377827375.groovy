import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), id)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > tr > td > a')

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/26.Edición de una acción/2.Editar'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/26.Edición de una acción/1.Editar'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/3.inputCheck_record'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/4.select_action_type'), 
    '1', true)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), 0)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/5.select_end_date_month'), 
    '12', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/6.select_end_date_day'), 
    '31', true)

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/7.textarea_resources'), 
    'Edicion de una Accion prueba katalon')

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/8.textarea_end_goal'), 
    'Edicion de una Accion prueba kataon')

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/9.input_hours'), 
    '75')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/10.input_initial_budget'), 
    '9000000')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/11.input_investment_budget'), 
    '7100000')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/12.input_operating_budget'), 
    '3800000')

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/13.inputCheck_exclude_consolidate'))

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/2.Agrgar Accion/14.btn_Save'))

