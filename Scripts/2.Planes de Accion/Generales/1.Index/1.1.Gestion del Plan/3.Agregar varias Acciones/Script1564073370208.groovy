import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.util.concurrent.locks.Condition as Condition
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.By.ById as ById
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.ui.Select as Select
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), id)

//WebUI.click(findTestObject('Generales/Select Item List/1.list_index'))
WebUI.delay(5)

//Analisis...
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/3.Agregar varias Acciones/1.btn_option_more'))

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/3.Agregar varias Acciones/2.btn_add_many_actions'))

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/3.Agregar varias Acciones/3.input_number_actions'), 
    '2')

//De acuerdo al numero seteado al input a si mismo cambiar la variable num
int num = 2

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/3.Agregar varias Acciones/4.btn_accept'))

WebUI.delay(5)

for (int i = 0; i < num; i++) {
    CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'), i)

    String text1 = ('ap_base_return_sub' + i) + '_action_type_id'

    WebDriver driver = DriverFactory.getWebDriver()

    Select obj1 = new Select(driver.findElement(By.id(text1)))

    obj1.selectByValue('1')

    String text2 = ('ap_base_return_sub' + i) + '_ini_date_month'

    Select obj2 = new Select(driver.findElement(By.id(text2)))

    obj2.selectByValue('12')

    String text3 = ('ap_base_return_sub' + i) + '_ini_date_day'

    Select obj3 = new Select(driver.findElement(By.id(text3)))

    obj3.selectByValue('3')

    WebDriver driver1 = DriverFactory.getWebDriver()

    String text4 = ('ap_base_return_sub' + i) + '_initial_budget'

    WebElement obj4 = driver1.findElement(By.id(text4))

    obj4.sendKeys('5000000')

    String text5 = ('ap_base_return_sub' + i) + '_investment_budget'

    WebElement obj5 = driver1.findElement(By.id(text5))

    obj5.sendKeys('5100000')

    String text6 = ('ap_base_return_sub' + i) + '_operating_budget'

    WebElement obj6 = driver1.findElement(By.id(text6))

    obj6.sendKeys('4800000')

    String text7 = ('ap_base_return_sub' + i) + '_exclude_consolidate'

    WebElement obj7 = driver1.findElement(By.id(text7))

    obj7.click()

    WebUI.delay(5)
}

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/3.Agregar varias Acciones/5.btn_Save'))
/// Script para eliminar cuando elhallazgos este ok
//WebUI.refresh()
//CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > .odd:nth-child(4) > td:nth-child(2) > a')