import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), id)

//WebUI.click(findTestObject('Generales/Select Item List/1.list_index'))
WebUI.delay(8)

CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > tr > td > a')
//CustomKeywords.'test.Metodos.select_items_actions'('#action-list-table > tbody > tr:nth-child(6) > td:nth-child(3) > a')
WebUI.delay(5)

CustomKeywords.'test.Metodos.select2_multiselect'(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/1.add_list'), 0)

//Cuando se aniade la lista se carga un nombre en el input del nombre de la lista, pero es posible sobreescribirlo. A continuacion queda el Input pero se dejara comentado...
//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Ejecutar Acciones/2.input_task_list_name'), 'Lista para pruebas...')
//Enviar un ENTER.SendKeys
WebElement enter = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/2.input_task_list_name'), 
    5)

enter.sendKeys(Keys.ENTER)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2_multiselect'(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/1.add_list'), 1)

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/4.input_task_notes'), 
    'Una tarea...')

WebElement enter2 = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/7.Crear Tareas del Plan de Accion/4.input_task_notes') , 
    5)

enter2.sendKeys(Keys.ENTER)

