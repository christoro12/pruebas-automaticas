import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/6.Filtro_Index/1filter'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/6.Filtro_Index/2select_type_plan'), '1', 
    true)

//WebUI.selectOptionByValue(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/3Select_Origin'), 
//'11', true)

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/4input_Lider'), 
    'fredy sandoval')

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/5input_responsible'), 
//'fredy sandoval')

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/6input_filtername'), 
//  'prueba')

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/7input_filternotes'), 
//'prueba')

//WebUI.selectOptionByValue(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/8select_financiacion'), 
//'11', true)

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/9input_Area_Origen'), 
//'prueba')

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/10input_filterprocess'), 
//'prueba origen')

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/9input_Area_Origen'), 
//'prueba area origen')

//WebUI.selectOptionByValue(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/11input_state'), 
//'4', true)

//WebUI.selectOptionByValue(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/12select_Si_no'), 
//'1', true)

//WebUI.selectOptionByValue(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/13select_year'), 
//'2019', true)

//WebUI.selectOptionByValue(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/14select_sistema_gestion'), 
//'23', true)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/6.Filtro_Index/15input_btn_save'))


