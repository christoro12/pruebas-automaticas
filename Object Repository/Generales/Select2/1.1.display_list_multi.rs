<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.1.display_list_multi</name>
   <tag></tag>
   <elementGuidId>55b74cf4-bff9-496a-88f6-9a0e7e62d806</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@class = 'select2-search-field']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2-search-field</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
</WebElementEntity>
