<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.btn_delete</name>
   <tag></tag>
   <elementGuidId>b38b2904-b207-40b0-aadd-5deafe32b667</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'btn btn-primary btn-circle btn-sm tq_various_confirm_standar' and @href = '/app.php/staff/ohsas_location/batch/action?batch_action=batchDelete&amp;ids=_#ids#_&amp;related_module=incident' and @role = 'button']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/div[1]/section/div[4]/div/a[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
