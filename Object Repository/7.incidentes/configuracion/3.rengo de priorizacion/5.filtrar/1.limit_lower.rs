<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.limit_lower</name>
   <tag></tag>
   <elementGuidId>489e66a2-bfc2-442c-a898-7287c6c37b88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#sf_admin_content > div > table > tbody > tr:nth-child(1) > td.sf_admin_text.sf_admin_list_td_lower_limit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
