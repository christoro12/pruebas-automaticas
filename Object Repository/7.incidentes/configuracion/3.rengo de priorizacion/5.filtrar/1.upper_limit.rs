<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.upper_limit</name>
   <tag></tag>
   <elementGuidId>5acb4184-aea0-470c-aa3f-b0286a169d83</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#sf_admin_content > div > table > tbody > tr:nth-child(1) > td.sf_admin_text.sf_admin_list_td_upper_limit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
