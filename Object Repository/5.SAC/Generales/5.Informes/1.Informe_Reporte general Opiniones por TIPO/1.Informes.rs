<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.Informes</name>
   <tag></tag>
   <elementGuidId>8189a3d7-8326-461e-8c61-bb048beffe50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/aside/div/section/ul/li[3]/ul/li[5]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/app.php/staff/wfi_opinion/indexReports' and @class = 'tq_self_none']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/app.php/staff/wfi_opinion/indexReports</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tq_self_none</value>
   </webElementProperties>
</WebElementEntity>
