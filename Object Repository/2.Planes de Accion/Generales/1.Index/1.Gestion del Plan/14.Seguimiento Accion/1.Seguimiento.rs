<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.Seguimiento</name>
   <tag></tag>
   <elementGuidId>32b405da-c4dc-4df9-9d0c-1e81310e43bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[@data-original-title = 'Seguimiento a la meta de cierre']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-original-title</name>
      <type>Main</type>
      <value>Seguimiento a la meta de cierre</value>
   </webElementProperties>
</WebElementEntity>
