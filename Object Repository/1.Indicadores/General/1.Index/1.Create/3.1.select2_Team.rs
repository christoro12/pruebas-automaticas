<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>3.1.select2_Team</name>
   <tag></tag>
   <elementGuidId>6d637aab-c2a2-4dc4-b867-9b5eff55f53e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#select2-result-label-45</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>#select2-results-1 > li:nth-child(1)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#select2-results-1 > li:nth-child(1)</value>
   </webElementProperties>
</WebElementEntity>
