<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>4.2.select2_Team</name>
   <tag></tag>
   <elementGuidId>da7fa3bc-a32c-48b3-a232-7e0dbc2d3675</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#select2-results-1 > li:nth-child(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#select2-results-1 > li:nth-child(2)</value>
   </webElementProperties>
</WebElementEntity>
