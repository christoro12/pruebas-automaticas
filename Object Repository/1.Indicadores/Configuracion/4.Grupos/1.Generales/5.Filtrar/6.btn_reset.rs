<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>6.btn_reset</name>
   <tag></tag>
   <elementGuidId>8da9891d-f62a-4750-b83c-e4705ff9916b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'btn-filter-reset btn btn-default btn-sm' and @href = '/app.php/staff/indicator_group/filter/action?_reset']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn-filter-reset btn btn-default btn-sm</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/app.php/staff/indicator_group/filter/action?_reset</value>
   </webElementProperties>
</WebElementEntity>
